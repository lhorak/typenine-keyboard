# Typenine Client app

## Development

1. Go to client folder 
    
    ```
    # cd client
    ```
    
2. Install dependencies 
    
    ```
    # yarn install --pure-lockfile
    ```
    
3. Start the server 
    
    ```
    # yarn start
    ```

## Test

1. Go to client folder
    
    ```
    # cd client
    ```

2. Make sure you have dependencies installed
    
    ```
    # yarn install --pure-lockfile
    ```
    
3. Run the tests 
    ```
    # yarn test
    ```