import React, { Component } from 'react';
import styled from 'styled-components';
import { getCurrentTime } from '../utils/time';
import Switch from './Switch';

const StyledHeader = styled.div`
    flex: 1 0 auto;
    background: #0B0B0B;
    color: white;
    display: grid;
    grid-template-columns: auto auto;
    height: 25px;
    font-size: 12px;
    line-height: 25px;
`;

const HeaderContent = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: ${props => props.justifyContent};
    padding: 0 10px;
`;

type Props = {
    smartSuggestEnabled: boolean,
    onSmartSuggestToggle: () => void
}

type State = {
    currentTime: {
        hours: number,
        minutes: number,
        seconds: number
    }
}

export default class Header extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            currentTime: getCurrentTime()
        }

    }


    timeInterval = setInterval(() => {
        this.setState({
            currentTime: getCurrentTime()
        });
    }, 500);

    componentWillUnmount() {
        clearInterval(this.timeInterval);
    }
    
    
    
    renderTime() {
        const { currentTime } = this.state;

        return `${currentTime.hours < 10 ? '0' : ''}${currentTime.hours}:${currentTime.minutes < 10 ? '0' : ''}${currentTime.minutes}`
    }

    render() {
        const { smartSuggestEnabled, onSmartSuggestToggle } = this.props;

        return (
        <StyledHeader>
            <HeaderContent justifyContent="flex-start" className="s-header-time">{this.renderTime()}</HeaderContent>
            <HeaderContent justifyContent="flex-end">
                <Switch 
                    title={'Smart suggest'}
                    defaultChecked={smartSuggestEnabled}
                    onClick={onSmartSuggestToggle} />
            </HeaderContent>
        </StyledHeader>
        )
    }
}
