import React, { Component } from 'react';
import styled from 'styled-components';
import SuggestionItem from './SuggestionItem';

const StyledSuggestionsContainer = styled.div`
    background: #C4C4C4;
    min-height: 40px;
    float:left;
    width: auto;

    /*These lines do the trick*/
    overflow-x: scroll;
    white-space: nowrap;
    width: 100%;
`;

const EmptyMessage = styled.div`
    width: 100%;
    height: 50px;
    text-align: center;
    color: white;
    display: flex;
    justify-content: center;
    flex-direction: column;
`;

type Props = {
    onSuggestionClick: (word: string) => void,
    suggestedWords: string[]
}

export default class Suggestions extends Component<Props> {
    onSuggestionClick = (word: string) => {
        this.props.onSuggestionClick(word);
    }

    renderSuggestedWords() {
        const { suggestedWords } = this.props;

        return suggestedWords.map((word, index) => {
            return (<SuggestionItem key={index} text={word} onClick={this.onSuggestionClick} />)
        });
    }

    render() {
        const { suggestedWords } = this.props;

        return (
            <StyledSuggestionsContainer>
                {
                    (suggestedWords && suggestedWords.length > 0) ? 
                    this.renderSuggestedWords() :
                    <EmptyMessage>Type to see suggestions</EmptyMessage>
                }
            </StyledSuggestionsContainer>
        )
    }
}
