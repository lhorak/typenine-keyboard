import React from 'react';
import styled from 'styled-components';


const StyledMessageBubble = styled.div`
    display: inline-block;
    max-width: 60%;
    border-radius: 20px;
    background: linear-gradient(#43cdf6,#0087fe);
    padding: 10px;
    color: white;
    box-sizing: border-box;
    overflow: hidden;
`;

type Props = {
    text: string
}

const MessageBubble = ({ text }: Props) => {
    return (
        <StyledMessageBubble>
            {text}
        </StyledMessageBubble>
    );
}

export default MessageBubble;
