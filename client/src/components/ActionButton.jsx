import React, { Component } from 'react';
import styled from 'styled-components';

const StyledActionButton = styled.span`
    display: inline-flex;
    justify-content: center;
    flex-direction: column;
    border-radius: 100%;
    width: 30px;
    height: 30px;
    text-align: center;
    margin: 5px 10px;
    font-size: 15px;
    font-weight: bold;
    cursor: pointer;
    background: ${props => props.type === 'send' ? '#0087fe' : 'transparent'};
    transition: all 300ms ease-out;
    color: ${props => props.type === 'send' ? 'white' : '#1C1C1C'};
    user-select: none;
`;

type Props = {
  onClick: () => void,
  title: string
}

const ActionButton = (props: Props) => {
  const { onClick, title } = props;
  return (
    <StyledActionButton onClick={onClick} {...props}>
      {title}
    </StyledActionButton>
  );
}

export default ActionButton;

