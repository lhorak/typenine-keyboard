import React, { Component } from 'react';
import styled from 'styled-components';

const StyledSwitch = styled.div`
    position: relative; width: 30px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
    display: inline-block;
    margin: 4px 0 0 0;
`;

const StyledInput = styled.input`
    display: none;
`;

const StyledLabel = styled.label`
    display: block; overflow: hidden; cursor: pointer;
    height: 12px; padding: 0; line-height: 12px;
    border: 2px solid #999999; border-radius: 12px;
    background-color: #EEEEEE;
    transition: background-color 0.3s ease-in;

    ${StyledInput}:checked + & {
        background-color: rgb(76, 217, 100);
    }

    ${StyledInput}:checked + &:before {
        right: 0px;
    }

    ${StyledInput}:checked + &, ${StyledInput}:checked + &:before {
        border-color: rgb(76, 217, 100);
    }

    &:before {
        content: "";
        display: block; width: 12px; height: 12px; margin: 0px;
        background: #FFFFFF;
        position: absolute; top: 0; bottom: 0;
        right: 14px;
        border: 2px solid #999999; border-radius: 12px;
        transition: all 0.3s ease-in 0s; 
    }
`;

const StyledTitle = styled.span`
    box-sizing: content-box;
    padding: 0 5px;
`;

type Props = {
    title: string,
    defaultChecked: boolean,
    onClick: (event: SyntheticMouseEvent<HTMLInputElement>) => void
}

class Switch extends Component<Props> {
    onSwitchClick = (event: SyntheticMouseEvent<HTMLInputElement>) => {
        event.preventDefault();
        this.props.onClick(event);
    }

    render() {
        return (
            <React.Fragment>
                <StyledTitle>{this.props.title}</StyledTitle>
                <StyledSwitch onClick={this.onSwitchClick}>
                    <StyledInput 
                        type="checkbox" 
                        name="onoffswitch" 
                        id="myonoffswitch" 
                        checked={this.props.defaultChecked} 
                        onChange={this.onSwitchClick} 
                    />
                    <StyledLabel htmlFor="myonoffswitch"></StyledLabel>
                </StyledSwitch>
            </React.Fragment>
        );
    }
}

export default Switch;
