import React, { Component } from 'react';
import styled from 'styled-components';
import MessageBubble from './MessageBubble';

const StyledMessages = styled.div`
    width: 100%;
    min-height: 240px;
    overflow: none;
    background: #000000;
    display:flex;
`;

const StyledMessagesInner = styled.div`
    width: 100%;
    overflow-y: scroll;
    background: #ffffff;
    border-radius: 10px 10px 0 0;
    padding: 10px 0;
`;

const MessageWrapper = styled.div`
    margin: 5px 10px;
    display: flex;
    justify-content: flex-end;
`;

type Props = {
    messages: string[]
}

export default class Messages extends Component<Props> {
    bottomRef: ?HTMLDivElement;

    scrollToBottom = () => {
        if (this.bottomRef) {
            this.bottomRef.scrollIntoView({ behavior: "smooth" });
        }
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        const { messages } = this.props;
        return (
        <StyledMessages>
            <StyledMessagesInner>
                {messages.map((message, index) => {
                    return (
                        <MessageWrapper key={index}>
                            <MessageBubble text={message} />
                        </MessageWrapper>
                    )
                })}
                <div ref={element => (this.bottomRef = element)}></div>
            </StyledMessagesInner>
        </StyledMessages>
        )
    }
}
