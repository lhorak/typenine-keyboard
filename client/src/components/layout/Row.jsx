import * as React from 'react';
import styled from 'styled-components';

const StyledRow = styled.div`
    display: flex;
    margin: 0;
    justify-content: space-between;
    box-sizing: border-box;
    flex: ${props => props.grow ? props.grow : 0} ${props => props.shrink ? props.shrink : 0} auto;
`;

type Props = {
    children?: React.Node,
    shrink?: number,
    grow?: number
}

const Row = ({ children, shrink = 0, grow = 0 }: Props) => {
    return (
        <StyledRow shrink={shrink} grow={grow}>
            {children}
        </StyledRow>
    );
}

export default Row;
