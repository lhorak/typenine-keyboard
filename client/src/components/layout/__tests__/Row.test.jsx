import React from 'react';
import { shallow, mount } from 'enzyme';
import Row from '../Row';


describe('Row component', () => {
  test('should render children inside component', () => {
    const component = mount(
        <Row>
            <span>Hello world</span>
        </Row>
    );

    expect(component.text()).toEqual('Hello world');
  });

  test('should render Row', () => {
    const component = mount(
        <Row></Row>
    );

    expect(component.html()).toMatchSnapshot();
  });
});
