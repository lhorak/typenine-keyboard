import React from 'react';
import { shallow, mount } from 'enzyme';
import Wrapper from '../Wrapper';

describe('Wrapper component', () => {
  test('should render children inside component', () => {
    const component = mount(
        <Wrapper>
            <span>Hello world</span>
        </Wrapper>
    );

    expect(component.text()).toEqual('Hello world');
  });

  test('should render Wrapper', () => {
    const component = mount(<Wrapper></Wrapper>);

    expect(component.html()).toMatchSnapshot();
  });
});
