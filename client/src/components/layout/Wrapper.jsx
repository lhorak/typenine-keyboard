import * as React from 'react';
import styled from 'styled-components';
import isMobileDevice from '../../utils/isMobile';

const StyledWrapper = styled.div`
    width: ${isMobileDevice() ? '100%': '375px'};
    height: 100vh;
    border-radius: ${isMobileDevice() ? 'none': '5px'};
    border: ${isMobileDevice() ? 'none' : '1px solid #C4C4C4'};
    margin: 0 auto;
    padding: 0 0 25px 0;
    display: flex;
    flex-direction: column;
    box-sizing: border-box;
    overflow: hidden;
`;

type Props = {
    children?: React.Node
}

const Wrapper = (props: Props) => {
    return (
        <StyledWrapper>
            {props.children}
        </StyledWrapper>
    );
}

export default Wrapper;
