import React from 'react';
import { shallow } from 'enzyme';
import Messages from '../Messages';
import MessageBubble from '../MessageBubble';


describe('Messages component', () => {
  test('should render empty container if no messages are present', () => {
    const component = shallow(<Messages messages={[]} />);

    expect(component).toMatchSnapshot();
  });

  test('should render messages', () => {
    const component = shallow(<Messages messages={['hello', 'world', 'i am here']} />);

    expect(component.find(MessageBubble)).toHaveLength(3);
    expect(component.html()).toMatchSnapshot();
  });

})
