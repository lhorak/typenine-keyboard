import React from 'react';
import { noop } from 'lodash';
import { mount } from 'enzyme';
import Keyboard from '../Keyboard';
import KeyboardButton from '../KeyboardButton';

describe('Keyboard component', () => {
    test('should render default keyboard layout', () => {
        expect(mount(<Keyboard onKeyClick={noop} />).html()).toMatchSnapshot();
    });

    test('should call onClick if key is clicked', () => {
        const clickSpy = jest.fn();
        const component = mount(<Keyboard onKeyClick={clickSpy} />);

        
        expect(component.find(KeyboardButton)).toHaveLength(12);

        component.find(KeyboardButton).at(1).simulate('click');
        expect(clickSpy).toHaveBeenCalled();
        expect(clickSpy).toHaveBeenCalledWith(2, "a b c", 'type');
    });
});
