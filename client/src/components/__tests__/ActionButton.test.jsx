import React from 'react';
import { shallow, mount } from 'enzyme';
import { noop } from 'lodash';
import ActionButton from '../ActionButton';

describe('ActionButton', () => {
  test('should render action button', () => {
    const component = mount(<ActionButton onClick={noop} title="action" />)

    expect(component.text()).toEqual('action');
  });
  
  test('should call onClick on click event', () => {
    const clickSpy = jest.fn();
    const component = shallow(<ActionButton onClick={clickSpy} title="action" />)

    component.simulate('click');
    expect(clickSpy).toHaveBeenCalled();
  });
})
