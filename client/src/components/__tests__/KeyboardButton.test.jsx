import React from 'react';
import { shallow, mount } from 'enzyme';
import KeyboardButton, {KeyboardNumber, KeyboardText} from '../KeyboardButton';
import { noop } from 'lodash';

describe('KeyboardButton component', () => {
    test('should render keyboard according to props', () => {
        const component = mount(<KeyboardButton digit="2" text="a b c" action="type" onClick={noop} />);

        expect(component.html()).toMatchSnapshot();
        expect(component.find(KeyboardNumber).text()).toEqual('2');
        expect(component.find(KeyboardText).text()).toEqual('a b c');
    });

    test('should call on click with given parameters', () => {
        const clickSpy = jest.fn();
        const component = shallow(<KeyboardButton digit="2" text="a b c" action="type" onClick={clickSpy} />);

        component.simulate('click', { stopPropagation() {} });

        expect(clickSpy).toHaveBeenCalledWith('2', 'a b c', 'type');
    });
});