import * as React from 'react';
import { noop } from 'lodash';
import { mount, shallow } from 'enzyme';
import Header from '../Header';
import Switch from '../Switch';

describe('Header component', () => {
    test('should render switch for smart suggest', () => {
        expect(shallow(<Header onSmartSuggestToggle={noop} smartSuggestEnabled={true} />).find(Switch).length).toEqual(1);
    });

    test('should call onSmartSuggestToggle on toggle click', () => {
        const clickSpy = jest.fn();
        const component = mount(<Header onSmartSuggestToggle={clickSpy} smartSuggestEnabled={true} />);

        component.find(Switch).simulate('click');
        expect(clickSpy).toHaveBeenCalled();
    });
});