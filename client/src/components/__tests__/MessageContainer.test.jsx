import React from 'react';
import { mount } from 'enzyme';
import MessageContainer from '../MessageContainer';


describe('MessageContainer component', () => {
  test('should render container with given message', () => {
    const component = mount(<MessageContainer message={'dummy text'} />)

    expect(component.text()).toEqual('dummy text');
  });

  test('should render placeholder if message is empty', () => {
    const component = mount(<MessageContainer message={''} />)

    expect(component.text()).toEqual('Start typing...');
  });
})
