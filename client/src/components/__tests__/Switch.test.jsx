import React from 'react';
import { shallow, mount } from 'enzyme';
import { noop } from 'lodash';
import Switch from '../Switch';


describe('Switch component', () => {
  test('should render switch with title', () => {
    const component = mount(<Switch title="Switch title" defaultChecked={true} onClick={noop} />);

    expect(component.text()).toEqual('Switch title');
  });

  test('should render switch with default checked value', () => {
    const component = mount(<Switch title="Switch title" defaultChecked={true} onClick={noop} />);

    expect(component.find('input').prop('checked')).toEqual(true);
  });

  test('should call onClick if the switch is clicked', () => {
    const clickSpy = jest.fn();
    const component = mount(<Switch title="Switch title" defaultChecked={true} onClick={clickSpy} />);

    component.find('input').simulate('click', { preventDefault() {}});
    expect(clickSpy).toHaveBeenCalled();
  });

});
