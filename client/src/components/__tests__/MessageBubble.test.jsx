import React from 'react';
import { mount } from 'enzyme';
import MessageBubble from '../MessageBubble';


describe('MessageBubble component', () => {
  test('should render message bubble with text', () => {
    const component = mount(<MessageBubble text={'dummy text'} />)

    expect(component.text()).toEqual('dummy text');
  });
})
