import React from 'react';
import { shallow, mount } from 'enzyme';
import { noop } from 'lodash';
import SuggestionItem from '../SuggestionItem';
import Suggestions from '../Suggestions';


describe('Suggestions component', () => {
  test('should render each suggestion', () => {
    const component = shallow(<Suggestions suggestedWords={['a', 'b', 'c']} onSuggestionClick={noop} />);

    expect(component.find(SuggestionItem)).toHaveLength(3);
  });

  test('should render placeholder if no suggestions are available', () => {
    const component = mount(<Suggestions suggestedWords={[]} onSuggestionClick={noop} />);

    expect(component.text()).toEqual('Type to see suggestions');
  });

  test('should call onSuggestionClick with given suggestion on click', () => {
    const clickSpy = jest.fn();
    const component = mount(<Suggestions suggestedWords={['a', 'b', 'c']} onSuggestionClick={clickSpy} />);

    component.find(SuggestionItem).at(1).simulate('click');
    expect(clickSpy).toHaveBeenCalledWith('b');
  });
});
