import React from 'react';
import { shallow, mount } from 'enzyme';
import { noop } from 'lodash';
import SuggestionItem from '../SuggestionItem';


describe('SuggestionItem component', () => {
  test('should render suggestion text', () => {
    const component = mount(<SuggestionItem text="suggestion" onClick={noop} />);

    expect(component.text()).toEqual('suggestion');
  });

  test('should handle onClick', () => {
    const clickSpy = jest.fn();
    const component = mount(<SuggestionItem text="suggestion" onClick={clickSpy} />);

    component.simulate('click');
    expect(clickSpy).toHaveBeenCalledWith('suggestion');
  });
});
