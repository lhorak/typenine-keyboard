import React, { Component } from 'react';
import styled from 'styled-components';

const StyledSuggestionContainer = styled.span`
    padding: 0 10px;
    border-right: 1px solid white;
    display: inline-block;
    line-height: 50px;
    color: white;
    cursor: pointer;
    user-select: none;
`;

type Props = {
    text: string,
    onClick: (text: string) => void
}

export default class SuggestionItem extends Component<Props> {
    onClick = () => {
        this.props.onClick(this.props.text);
    }

    render() {
        return (
            <StyledSuggestionContainer onClick={this.onClick}>
                {this.props.text}
            </StyledSuggestionContainer>
        )
    }
}
