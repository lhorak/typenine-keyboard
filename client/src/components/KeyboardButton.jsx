// @flow
import React, { Component } from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  width: 70px;
  height: 70px;
  border-radius: 50%;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border: 1px solid black;
  padding: 12px 12px 18px 12px;
  margin: 5px;
  cursor: pointer;
  background: transparent;
  transition: all 300ms ease-out;
  outline: none;
  touch-action: none;

  @media (hover: hover) {
    &:hover {
      background: #F6F6F6;
    }
  }

`;

export const KeyboardNumber = styled.span`
  font-size: 30px;
  font-weight: bold;
  text-align: center;
`;

export const KeyboardText = styled.span`
  font-size: 10px;
  font-weight: 500;
  text-align: center;
  text-transform: uppercase;
`;

export type DigitType = string | number;
export type ActionType = string | null;

type Props = {
  digit: DigitType,
  text: string,
  action: ActionType,
  onClick: (keyNumber: DigitType, text: string, action: ActionType) => void;
};

export default class KeyboardButton extends Component<Props> {
  static defaultProps = {
    text: '',
    action: null
  }

  onClick = (event: SyntheticMouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
    const { digit, text, action } = this.props;
    this.props.onClick(digit, text, action);
  }

  render() {
    const { digit, text } = this.props;

    return (
      <StyledButton onClick={this.onClick}>
        <KeyboardNumber>{digit}</KeyboardNumber>
        <KeyboardText>{text}</KeyboardText>
      </StyledButton>
    )
  }
}
