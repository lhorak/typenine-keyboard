// @flow
import React, { Component } from 'react';
import styled from 'styled-components';
import KeyboardButton from './KeyboardButton';
import type { DigitType, ActionType } from './KeyboardButton';


const KeyboardLayout = styled.div`
    display: grid;
    grid-template-columns: auto auto auto;
    align-items: center;
    max-width: 300px;
    margin: 10px auto 0 auto;
`;

const KeyWrapper = styled.div`
    margin: 0 auto;
`;

const defaultKeys = [
    { digit: 1, text: '', action: null},
    { digit: 2, text: 'a b c', action: 'type'},
    { digit: 3, text: 'd e f', action: 'type'},
    { digit: 4, text: 'g h i', action: 'type'},
    { digit: 5, text: 'j k l', action: 'type'},
    { digit: 6, text: 'm n o', action: 'type'},
    { digit: 7, text: 'p q r s', action: 'type'},
    { digit: 8, text: 't u v', action: 'type'},
    { digit: 9, text: 'w x y z', action: 'type'},
    { digit: '✱', text: '', action: null},
    { digit: 0, text: '+', action: null},
    { digit: '⌫', text: '', action: 'delete'}
];

type Props = {
    onKeyClick: (digit: DigitType, text: string, action: ActionType) => void
};

export default class Keyboard extends Component<Props> {
  render() {
    const { onKeyClick } = this.props;

    return (
    <div>
        <KeyboardLayout>
            {defaultKeys.map(key => {
                return (
                    <KeyWrapper key={key.digit}>
                        <KeyboardButton digit={key.digit} text={key.text} action={key.action} onClick={onKeyClick} />
                    </KeyWrapper>
                );
            })}
        </KeyboardLayout>
    </div>
    )
  }
}
