import React from 'react';
import styled from 'styled-components';

const StyledMessageContainer = styled.div`
    border-radius: 15px;
    border: 1px solid #404040;
    min-height: 30px;
    max-height: 90px;
    line-height: 30px;
    max-width: calc(100% - 70px);
    padding: 0 10px;
    flex: 1 0 auto;
    box-sizing: border-box;
    margin: 5px 10px;
    overflow-y: scroll;
`;

const Placeholder = styled.span`
    color: #E6E6E6;
`;

type Props = {
    message: string
}

const MessageContainer = ({ message }: Props) => {
    return (
        <StyledMessageContainer>
            {(message && message.length > 1) ? message : <Placeholder>Start typing...</Placeholder>}
        </StyledMessageContainer>
    );
}

export default MessageContainer;
