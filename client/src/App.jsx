// @flow
import React, { Component } from 'react';
import { debounce } from 'lodash';
import Keyboard from './components/Keyboard';
import ActionButton from './components/ActionButton';
import Wrapper from './components/layout/Wrapper';
import Row from './components/layout/Row';
import Suggestions from './components/Suggestions';
import Messages from './components/Messages';
import MessageContainer from './components/MessageContainer';
import Header from './components/Header';
import { getTypenine } from './utils/api';
import type { DigitType, ActionType } from './components/KeyboardButton';

type Props = {};
type State = {
  phoneword: string,
  message: string,
  messages: string[],
  suggestions: string[],
  smartSuggest: boolean
}

class App extends Component<Props, State> {
  state = {
    phoneword: '',
    message: '',
    messages: [],
    suggestions: [],
    smartSuggest: true
  }

  onKeyClick = (digit: DigitType, text: string, action: ActionType) => {
    if (action === 'type') {
      const newPhoneword = this.state.phoneword + digit;
      this.setState({
        phoneword: newPhoneword
      });

      setTimeout(() => {
        this.getDebouncedSuggestions(newPhoneword);
      }, 0);

      return;
    }

    if (action === 'delete') {
      this.onDeleteClick();
    }

  }

  chooseWord = (word: string) => {
    this.setState({
      phoneword: '',
      message: `${this.state.message}${word} `,
      suggestions: []
    })
  }

  onDeleteClick = () => {
    const { phoneword, message } = this.state;

    const newPhoneword = phoneword.slice(0, -1);

    if (phoneword.length > 0) {
      this.setState({
        phoneword: newPhoneword
      });

      setTimeout(() => {
        this.getDebouncedSuggestions(newPhoneword);
      }, 0);

      return;
    }

    if (message.length > 0) {
      const trimmedText = message.slice(0, -1);

      this.setState({
        message: trimmedText
      });
    }
  }

  onSmartSuggestToggle = () => {
    this.setState({
      smartSuggest: !this.state.smartSuggest
    })
  }

  sendMessage = () => {
    const { messages, message, phoneword, suggestions } = this.state;

    if (phoneword.length > 0 && suggestions.length > 0) {
      this.setState({
        messages: [...messages, `${message} ${suggestions[0]}`],
        message: '',
        phoneword: '',
        suggestions: []
      });

      return;
    }

    if (message.length > 0) {
      this.setState({
        messages: [...messages, message],
        message: '',
        phoneword: ''
      });
    }
  }

  getSuggestions = (phoneword: string) => {
    const { smartSuggest } = this.state;

    getTypenine(phoneword, smartSuggest)
      .then(body => {
          this.setState({
          suggestions: body.suggestions
          })
      });
  };

  getDebouncedSuggestions = debounce(this.getSuggestions, 300);

  render() {
    const { message, suggestions, messages } = this.state;

    return (
      <Wrapper>
        <Row>
          <Header 
            onSmartSuggestToggle={this.onSmartSuggestToggle}
            smartSuggestEnabled={this.state.smartSuggest}
          />
        </Row>
        <Row shrink={1} grow={1}>
          <Messages messages={messages} />
        </Row>
        <Row shrink={0} grow={0}>
          <MessageContainer message={`${message} ${suggestions && suggestions.length > 0 ? suggestions[0] : ''}`} />
          <ActionButton title={'➤'} type={'send'} onClick={this.sendMessage}/>
        </Row>
        <Row>
          <Suggestions suggestedWords={this.state.suggestions} onSuggestionClick={this.chooseWord} />
        </Row>
        <Keyboard onKeyClick={this.onKeyClick}/>
      </Wrapper>
    );
  }
}

export default App;
