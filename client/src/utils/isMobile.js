export default function isMobileDevice() {
    if (window && navigator) {
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
    }

    return false;
};