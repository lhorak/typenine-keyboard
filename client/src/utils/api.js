const TYPENINE_API_ENDPOINT = '/api/typenine';

export function getTypenine(phoneword: string = '', smartSuggest: boolean = true) {
    const body = JSON.stringify({
        phoneword,
        smartSuggest
    });

    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body
    }


    return fetch(TYPENINE_API_ENDPOINT, config)
        .then(response => response.json());
}

const TypenineApi = {
    getTypenine
}

export default TypenineApi;