import { getTypenine } from '../api';

global.fetch = jest.fn().mockImplementation(() => {
    var p = new Promise((resolve, reject) => {
        resolve({
            ok: true, 
            json: function() { 
                return {suggestions: ['a', 'b', 'c']}
            }
        });
    });

    return p;
});

describe('API', () => {
    test('shuld get typenine from backend', async () => {
        const res = await getTypenine('dummy', true);

        expect(res).toEqual({
            suggestions: ['a', 'b', 'c']
        });
    });
});
