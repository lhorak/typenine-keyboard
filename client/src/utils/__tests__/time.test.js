import { getCurrentTime } from '../time';

describe('GetCurrentTime', () => {
    test('should return current time', () => {
        const timeToMatch = new Date();
        const currentTime = getCurrentTime();

        expect(currentTime.hours).toEqual(timeToMatch.getHours());
        expect(currentTime.minutes).toEqual(timeToMatch.getMinutes());
        expect(currentTime.seconds).toEqual(timeToMatch.getSeconds());
    });
});
