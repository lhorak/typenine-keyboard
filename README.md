# Typenine
> The ultimate keyboard to type on in 2019!

## Prerequisities

To run this app, make sure you have these on your machine:

- Yarn package manager (`yarn`)
- Current version of Node.js
- Web browser or smartphone to test the app on (with javascript enabled)

## Where to see the app

You can run the typenine app both in desktop and mobile browser, but I suggest you run it
as a PWA (progressive web app). This basically means, taht you build and run the app on desktop and
then you open the website (`http://<your-computer-local-ip-address>:3001`) and save the app on 
your mobile desktop (see the gif below for iOS). Then open the app.

### Adding the app as PWA on iOS

![Typenine add application as PWA on iOS](https://media.giphy.com/media/bE3viBbK1fSsGcqour/giphy.gif "Typenine add as PWA")

### Example of Typenine as PWA

![Example of Typenine as PWA on iOS](https://media.giphy.com/media/27bK3RRHrD28B2zQnj/giphy.gif "Example Typenine as PWA")

## Production (running the app)

To build the app for production and run it, please use provided shell script

### How to build and run the production app
> The production app runs on port 3001 and the port is not changable for now. Sorry for the inconvenience.
Please make sure this port is free

**Go to the project root of cloned repository**

Make sure the production shell script is executable

```
# chmod +x run-production.sh
```

**Execute the shell script to start the production server**

```
# ./run-production.sh
```

This will take a while especially if the dependencies were not previously installed.

The app should be running on `http://localhost:3001`

## Development

To run the application in development mode, youfirst need to clone this repository:

```
# git clone git@gitlab.com:lhorak/typenine-keyboard.git
```

If you have the repository cloned, there are two options how to run the development locally:

1. Using the provided shell script `run-development.sh`
2. Running both client and server separately

### Using the shell script

**Make sure that the script is able to be executed**

```
# chmod +x run-development.sh
```
*Now simply run*

```
# ./run-development.sh
```

The script shuld install dependencies and start both server and client app.
The client app runs on port `3000` and the server on port `3001`


Go to `http://localhost:3000` to see the app

> NOTE: The client app uses proxy to pass all the api requests to port 3001, 
so make sure both ports are available on your machine

### Runing both server and client separately

**Server**
1. Go to server folder
    
    ```
    # cd server
    ```

2. Install dependencies
    
    ```
    # yarn install --pure-lockfile
    ```
    
3. Start the server 
    ```
    # yarn start
    ```


**Client**
1. Go to client folder 
    
    ```
    # cd client
    ```
    
2. Install dependencies 
    
    ```
    # yarn install --pure-lockfile
    ```
    
3. Start the server 
    
    ```
    # yarn start
    ```


### Running tests

To run tests, you need to go to either client or server and run the tests in desired folder

### Runing both server and client separately

**Server**

1. Go to server folder
    
    ```
    # cd server
    ```

2. Make sure you have dependencies installed
    
    ```
    # yarn install --pure-lockfile
    ```
    
3. Run the tests 
    ```
    # yarn test
    ```


**Client**

1. Go to client folder
    
    ```
    # cd client
    ```

2. Make sure you have dependencies installed
    
    ```
    # yarn install --pure-lockfile
    ```
    
3. Run the tests 
    ```
    # yarn test
    ```

## Contact

In case of emergency, you can send me an email to lhorakcz@gmail.com