#!/bin/sh

echo 'Typenine dev'
SERVER_DIRECTORY=$PWD/server
CLIENT_DIRECTORY=$PWD/client

echo $SERVER_DIRECTORY
echo $CLIENT_DIRECTORY

run_server() {
    echo [typenine] starting server
    cd $SERVER_DIRECTORY
    yarn start &>/dev/null
    echo [Typenine] Server has started
}

run_client() {
    echo [typenine] starting client
    cd $CLIENT_DIRECTORY
    yarn start
    echo [Typenine] Client has started
}
cleanup() {
        local pids=$(jobs -pr)
        [ -n "$pids" ] && kill $pids
}


trap "exit" INT QUIT TERM
trap "kill 0" EXIT

run_server &
run_client &

wait