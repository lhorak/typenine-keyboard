# Typenine server

## Development

1. Go to server folder
    
    ```
    # cd server
    ```

2. Install dependencies
    
    ```
    # yarn install --pure-lockfile
    ```
    
3. Start the server 
    ```
    # yarn start
    ```

## Test

1. Go to server folder
    
    ```
    # cd server
    ```

2. Make sure you have dependencies installed
    
    ```
    # yarn install --pure-lockfile
    ```
    
3. Run the tests 
    ```
    # yarn test
    ```