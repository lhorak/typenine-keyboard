import { isValidPhoneword, splitPhonewordToDigits } from '../utils';

function typenineValidationMiddleware(req, res, next) {
    const phoneword = getPhonewordFromRequest(req);

    if (!isValidPhoneword(phoneword)) {
        return res.status(400).json({
            error: `${phoneword} in not a valid phoneword. Phoneword must be either string or number with digits in range [2-9]`
        });
    }

    const digits = splitPhonewordToDigits(phoneword);

    if (digits.length > 10 && !isSmartSuggestRequest(req)) {
        return res.status(400).json({
            error: "Sorry, but this is way too large to compute. Try POST with smartSuggest"
        });
    }

    next();
}

export function getPhonewordFromRequest(request) {
    if (request.method === 'GET') {
        return request.params.phoneword;
    }

    if (request.method === 'POST') {
        return request.body.phoneword;
    }

    return null;
}

export function isSmartSuggestRequest(request) {
    return request.method === 'POST' && request.body.smartSuggest
}

export default typenineValidationMiddleware;