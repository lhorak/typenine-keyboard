import { isSmartSuggestRequest, getPhonewordFromRequest } from '../typenineValidation';

describe('typenineValidation', () => {
  describe('isSmartSuggestRequest', () => {
    test('should return false if the request is GET', () => {
        const req = {
            method: 'GET'
        };

        expect(isSmartSuggestRequest(req)).toBeFalsy();
    });

    test('should return false if the request is POST but smartSuggest is not provided', () => {
        const req = {
            method: 'POST',
            body: {
                phoneword: '2'
            }
        };

        expect(isSmartSuggestRequest(req)).toBeFalsy();
    });

    test('should return true if the request is POST and smartSuggest is provided', () => {
        const req = {
            method: 'POST',
            body: {
                phoneword: '2',
                smartSuggest: true
            }
        };

        expect(isSmartSuggestRequest(req)).toBeTruthy();
    });
  });

  describe('getPhonewordFromRequest', () => {
      test('should get phoneword from GET request', () => {
          const req = {
              method: 'GET',
              params: {
                  phoneword: 2
              }
          };

          expect(getPhonewordFromRequest(req)).toEqual(2);
      });

      test('should get phoneword from POST request', () => {
        const req = {
            method: 'POST',
            body: {
                phoneword: '2'
            }
        };

        expect(getPhonewordFromRequest(req)).toEqual('2');
    });
  });
})
