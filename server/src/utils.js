export function isNumeric(str) {
    const reg = /^\d+$/;

    return reg.test(str);
};

export function isNumber(value) {
    return typeof value === 'number';
}

export function isString(value) {
    return typeof value === 'string';
}

export function isValidPhoneword(phoneword) {
    const phonewordRegex = /^[2-9]+$/;

    if (isNumber(phoneword) || isString(phoneword)) {
        return phonewordRegex.test(phoneword);
    }

    return false;
}

export function splitPhonewordToDigits(phoneword) {
    if (isValidPhoneword(phoneword)) {
        return isNumber(phoneword) ? phoneword.toString().split('') : phoneword.split('');
    }

    return [];
}