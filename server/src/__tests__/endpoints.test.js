import supertest from 'supertest';
import app from '../app';
import NUMPAD from '../typenine/numpad';

const server = supertest.agent(app);

describe('Endpoints', () => {
    describe('Typenine GET endpoint', () => {
        test('Should return all possibilities for one digit', async () => {
            const numpadNumber = 2;
            const response = await server.get(`/api/typenine/${numpadNumber}`);
            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual({
                suggestions: NUMPAD[numpadNumber]
            });
        });

        test('Should return all possibilities for two digits', async () => {
            const numpadNumber = 23;
            const response = await server.get(`/api/typenine/${numpadNumber}`);
            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual({
                suggestions: ['ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf']
            });
        });

        test('Should return error when phoneword is longer than 10', async () => {
            const numpadNumber = 23756438542;
            const response = await server.get(`/api/typenine/${numpadNumber}`);
            expect(response.statusCode).toEqual(400);
            expect(response.body).toEqual({
                error: 'Sorry, but this is way too large to compute. Try POST with smartSuggest'
            });
        });

        test('Should return error when the phoneword is not valid', async () => {
            const numpadNumber = 'hello';
            const response = await server.get(`/api/typenine/${numpadNumber}`);
            expect(response.statusCode).toEqual(400);
            expect(response.body).toEqual({
                error: `${numpadNumber} in not a valid phoneword. Phoneword must be either string or number with digits in range [2-9]`
            });
        });

        test('Should return error when the phoneword contains non phoneword digits', async () => {
            const numpadNumber = 301;
            const response = await server.get(`/api/typenine/${numpadNumber}`);
            expect(response.statusCode).toEqual(400);
            expect(response.body).toEqual({
                error: `${numpadNumber} in not a valid phoneword. Phoneword must be either string or number with digits in range [2-9]`
            });
        });

        test('Should return error when the phoneword is not present', async () => {
            const response = await server.get('/api/typenine');
            expect(response.statusCode).toEqual(400);
            expect(response.body).toEqual({
                error: 'You need to provide phoneword to convert. (/api/typenine/<phoneword>).'
            });
        });
    });

    describe('Typenine POST endpoint', () => {
        test('should by default work as GET requests', async () => {
            const numpadNumber = 2;
            const response = await server.post('/api/typenine').send({
                phoneword: numpadNumber
            });

            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual({
                suggestions: NUMPAD[numpadNumber]
            });
        });

        test('should work with phoneword as string', async () => {
            const numpadNumber = '2';
            const response = await server.post('/api/typenine').send({
                phoneword: numpadNumber
            });
            
            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual({
                suggestions: NUMPAD[numpadNumber]
            });
        });

        test('should return correct phonewords', async () => {
            const numpadNumber = '23';
            const response = await server.post('/api/typenine').send({
                phoneword: numpadNumber
            });
            
            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual({
                suggestions: ['ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf']
            });
        });

        test('should return error when the phoneword is over length limit', async () => {
            const numpadNumber = '23756463842';
            const response = await server.post('/api/typenine').send({
                phoneword: numpadNumber
            });
            
            expect(response.statusCode).toEqual(400);
            expect(response.body).toEqual({
                error: 'Sorry, but this is way too large to compute. Try POST with smartSuggest'
            });
        });

        test('should return error when the phoneword is not valid', async () => {
            const numpadNumber = '2041';
            const response = await server.post('/api/typenine').send({
                phoneword: numpadNumber
            });
            
            expect(response.statusCode).toEqual(400);
            expect(response.body).toEqual({
                error: '2041 in not a valid phoneword. Phoneword must be either string or number with digits in range [2-9]'
            });
        });

        test('should return error when the phoneword is not number', async () => {
            const numpadNumber = 'hello';
            const response = await server.post('/api/typenine').send({
                phoneword: numpadNumber
            });
            
            expect(response.statusCode).toEqual(400);
            expect(response.body).toEqual({
                error: 'hello in not a valid phoneword. Phoneword must be either string or number with digits in range [2-9]'
            });
        });

        test('should return smartSuggest words when the option is enambled', async () => {
            const numpadNumber = '43556';
            const response = await server.post('/api/typenine').send({
                phoneword: numpadNumber,
                smartSuggest: true
            });
            
            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual({
                suggestions: ['hello']
            });
        });

        test('should return smartSuggest words which are even longer than given phoneword', async () => {
            const numpadNumber = '822';
            const response = await server.post('/api/typenine').send({
                phoneword: numpadNumber,
                smartSuggest: true
            });
            
            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual({
                suggestions: ['tab', 'tba', 'tabs', 'table', 'tables', 'tablet', 'tackle', 'vacuum', 'tablets', 'tactics', 'vaccine', 'vacation', 'vacancies', 'vacations']
            });
        });

        test('should return empty array if smartSuggestion has no matches', async () => {
            const numpadNumber = '822895734';
            const response = await server.post('/api/typenine').send({
                phoneword: numpadNumber,
                smartSuggest: true
            });
            
            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual({
                suggestions: []
            });
        });
    });
});