import { 
  isNumber,
  isString,
  isNumeric,
  isValidPhoneword, 
  splitPhonewordToDigits
} from '../utils';

describe('Type validators', () => {
  test('should validate number type', () => {
    expect(isNumber(111)).toBeTruthy();
    expect(isNumber('111')).toBeFalsy();
    expect(isNumber({})).toBeFalsy();
    expect(isNumber('12a')).toBeFalsy();
    expect(isNumber(11.21)).toBeTruthy();
  });
  
  test('should validate string type', () => {
    expect(isString(111)).toBeFalsy();
    expect(isString('111')).toBeTruthy();
    expect(isString({})).toBeFalsy();
    expect(isString('12a')).toBeTruthy();
    expect(isString(11.21)).toBeFalsy();
    expect(isString(['a'])).toBeFalsy();
  });
  
  test('shuld validate that given value is integer (even strings)', () => {
    expect(isNumeric(111)).toBeTruthy();
    expect(isNumeric('111')).toBeTruthy();
    expect(isNumeric('1')).toBeTruthy();
    
    expect(isNumeric(111.11)).toBeFalsy();
    expect(isNumeric('')).toBeFalsy();
    expect(isNumeric('11a')).toBeFalsy();
    expect(isNumeric('11a11')).toBeFalsy();
    expect(isNumeric('a11')).toBeFalsy();
    expect(isNumeric('11.2')).toBeFalsy();
  
    expect(isNumeric({})).toBeFalsy();
    expect(isNumeric([])).toBeFalsy();
    expect(isNumeric(null)).toBeFalsy();
    expect(isNumeric(undefined)).toBeFalsy();
  });
});

describe('isValidPhoneword', () => {
  test('should accept number', () => {
    expect(isValidPhoneword(234)).toBeTruthy();
  });

  test('should accept string', () => {
    expect(isValidPhoneword('234')).toBeTruthy();
  });

  test('should decline number with 0 or 1 present', () => {
    expect(isValidPhoneword(20)).toBeFalsy();
    expect(isValidPhoneword(21)).toBeFalsy();
    expect(isValidPhoneword(0)).toBeFalsy();
    expect(isValidPhoneword(1)).toBeFalsy();
  });

  test('should decline string with 0 or 1 present', () => {
    expect(isValidPhoneword('20')).toBeFalsy();
    expect(isValidPhoneword('21')).toBeFalsy();
    expect(isValidPhoneword('0')).toBeFalsy();
    expect(isValidPhoneword('1')).toBeFalsy();
    expect(isValidPhoneword('357643503')).toBeFalsy();
  });

  test('should decline primitive types other than string or number', () => {
    expect(isValidPhoneword({})).toBeFalsy();
    expect(isValidPhoneword(['23'])).toBeFalsy();
  });

  test('should decline non digit strings', () => {
    expect(isValidPhoneword('phoneword')).toBeFalsy();
  });
});

describe('Split phoneword to digits', () => {
  test('should return array of digits from number', () => {
    expect(splitPhonewordToDigits(234)).toEqual(['2', '3', '4']);
  });

  test('should return array of digits from string', () => {
    expect(splitPhonewordToDigits('234')).toEqual(['2', '3', '4']);
  });

  test('should return array of digits from one number', () => {
    expect(splitPhonewordToDigits(2)).toEqual(['2']);
  });

  test('should return empty array for any invalid input', () => {
    expect(splitPhonewordToDigits(0)).toEqual([]);
    expect(splitPhonewordToDigits(1)).toEqual([]);
    expect(splitPhonewordToDigits({})).toEqual([]);
    expect(splitPhonewordToDigits(['2', '3'])).toEqual([]);
  });
});

