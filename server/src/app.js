
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import { getTypenine } from './typenine/typenine';
import SmartSuggest from './typenine/SmartSuggest';
import { splitNumberToDigits, isValidPhoneword, splitPhonewordToDigits } from './utils';
import typenineValidationMiddleware from './middleware/typenineValidation';

const app = express();
const router = express.Router();

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(express.static(`${__dirname}/../../client/build/`));
app.use('/api', router);

const smartSuggest = new SmartSuggest();

app.get('/', function(_req, res) {
    res.sendFile(path.join(__dirname, '/../../client/build/index.html'));
});


router.post('/typenine', typenineValidationMiddleware);
router.get('/typenine/:phoneword', typenineValidationMiddleware);

router.get('/typenine/:phoneword', (req, res) => {
    const { phoneword } = req.params;
    
    res.json({
        suggestions: getTypenine(phoneword)
    })
});

router.post('/typenine', (req, res) => {
    const { body } = req;
    
    if (body.smartSuggest) {
        return res.json({
            suggestions: smartSuggest.getSuggestions(body.phoneword)
        });
    }
    
    res.json({
        suggestions: getTypenine(body.phoneword)
    })
});

router.get('/typenine', (_req, res) => {
    res.status(400).json({
        error: 'You need to provide phoneword to convert. (/api/typenine/<phoneword>).'
    });
})

module.exports = app;
