import {generatePhonewords, getTypenine} from '../typenine';

describe('typenine functions', () => {
    describe('generatePhonewords', () => {
        test('should generate phonewords from one digit', () => {
            expect(generatePhonewords(['2'])).toEqual(['a', 'b', 'c']);
        });

        test('should generate phonewords from array of digits', () => {
            expect(generatePhonewords(['2', '3'])).toEqual(['ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf']);
        });
    });

    describe('getTypenine', () => {
        test('should generate phonewords from one digit string', () => {
            expect(getTypenine('2')).toEqual(['a', 'b', 'c']);
        });

        test('should generate phonewords from multiple digits string', () => {
            expect(getTypenine('23')).toEqual(['ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf']);
        });
    });
    
});