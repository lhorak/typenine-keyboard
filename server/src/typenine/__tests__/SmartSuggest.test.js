import SmartSuggest from '../SmartSuggest';

describe('SmartSuggest', () => {
    const smartSuggest = new SmartSuggest();

    it('should suggest words from default dictionary', () => {
        expect(smartSuggest.getSuggestions(43556)).toEqual(['hello']);
    });

    test('should filter dicitionary according to given array of digits', () => {
        expect(smartSuggest.filterDictionary(['4', '3', '5', '5', '6'])).toEqual(['hello']);
    });
});
