import fs from 'fs';
import { head, sortBy, drop } from 'lodash';
import NUMPAD from './numpad';

class SmartSuggest {
    constructor(dictionaryFilepath = `${__dirname}/../assets/dictionary.txt`, separator = '\n') {
        const fileContents = fs.readFileSync(dictionaryFilepath, { encoding: 'utf8' });
        this.dictionary = fileContents.split(separator).sort();
    }

    filterDictionary(numbers, index = 0, result = this.dictionary) {
        if (numbers.length === 0) {
            return null;
        }
    
        const currentLetters = NUMPAD[head(numbers)];
    
        const filteredDictionary = result.filter(word => currentLetters.some(letter => letter === word.charAt(index)));
    
        const nextNumbers = drop(numbers);
    
        if (nextNumbers.length === 0) {
            return sortBy(filteredDictionary.sort(), word => word.length);
        }
    
        return this.filterDictionary(nextNumbers, ++index, filteredDictionary);
    }

    getSuggestions(number) {
        return this.filterDictionary(number.toString().split(''));
    }
}

export default SmartSuggest;