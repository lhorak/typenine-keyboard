import {head, drop, flatten} from 'lodash';
import { splitPhonewordToDigits } from '../utils';
import NUMPAD from './numpad';

export function generatePhonewords(numbers= []) {
    // if there is only one number, return letters representing given number
    if (numbers.length === 0) {
        return null;
    }

    if (numbers.length === 1) {
        return NUMPAD[numbers[0]];
    }

    const phonewords = NUMPAD[head(numbers)];
    return drop(numbers).reduce((phonewords, number) => {
        return flatten(phonewords.map(word => {
            return NUMPAD[number].map(letter => word + letter);
        }));
    }, phonewords);
};

export function getTypenine(phoneword) {
    const digits = splitPhonewordToDigits(phoneword);
    return generatePhonewords(digits);
}
