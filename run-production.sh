#!/bin/sh

echo '[Typenine] Production'
SERVER_DIRECTORY=$PWD/server
CLIENT_DIRECTORY=$PWD/client

build_server() {
    if [ -d "$SERVER_DIRECTORY/lib" ]; then
        printf "\n[typenine] removing old build of server\n"
        rm -rf $SERVER_DIRECTORY/lib
    fi

    cd $SERVER_DIRECTORY
    printf "\n[Typenine] Installing dependencies\n"
    yarn install --pure-lockfile

    echo "\n[Typenine] Buliding server production code\n"
    yarn run build
    echo "\n[Typenine] Production files built in $SERVER_DIRECTORY/lib\n"
}

build_client() {
    printf "\n[typenine] building production version of client\n"
    cd $CLIENT_DIRECTORY

    if [ -d "$CLIENT_DIRECTORY/bulid" ]; then
        printf "\n[typenine] removing old build of client\n"
        rm -rf $SERVER_DIRECTORY/build
    fi

    printf "\n[Typenine] Installing dependencies/n"
    yarn install --pure-lockfile

    printf "\n[Typenine] Buliding client production code\n"
    yarn run build
    printf "\n[Typenine] Client production files built in $CLIENT_DIRECTORY/lib\n"
}

start_production_server() {
    cd $SERVER_DIRECTORY/lib
    node ./server.js
}

build_server
build_client
printf "\n\n[Typenine] Starting production server\n\n"
start_production_server